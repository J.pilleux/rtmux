use tmux_interface::{RenameWindow, SendKeys};

use crate::{conf::TmuxConf, tmux_wrapper::{hsplit, vsplit, resize_up}, validation::{validator::LIST_SPLIT, validator_helper::{HORIZONTAL, VERTICAL}}};
use std::{thread, time};

use super::tmux_wrapper::{tmux_cmd, tmux_rename};

const WAIT_MILIS: time::Duration = time::Duration::from_millis(500);

const INIT_NB_PANES: usize = 0;

pub struct TmuxCommand {
    conf: TmuxConf,
    nb_panes: usize,
}

impl TmuxCommand {
    pub fn new(conf: TmuxConf) -> TmuxCommand {
        return TmuxCommand {
            conf,
            nb_panes: INIT_NB_PANES,
        };
    }

    pub fn run(&mut self) {
        self.spawn_panes();
        self.resize_panes();
        self.ssh_panes();
        self.run_commands();
    }

    fn spawn_panes(&mut self) {
        if self.conf.layout.contains(LIST_SPLIT) {
            let values: Vec<&str> = self.conf.layout.split(LIST_SPLIT).collect();
            self.nb_panes = values.len() + 1;
            for split in values {
                self.split_pane(split);
            }
            thread::sleep(WAIT_MILIS);
        }
    }

    fn split_pane(&self, layout: &str) {
        if layout.contains(&self.conf.delimiter) {
            let values: Vec<&str> = layout.split(&self.conf.delimiter).collect();
            let dir = String::from(values[0]).to_lowercase();
            match dir.as_str() {
                HORIZONTAL => hsplit(values[1]),
                VERTICAL => vsplit(values[1]),
                _ => eprintln!("The split {} is not a valid value.", dir),
            };
        }
    }

    fn resize_panes(&self) {
        for resize_string in &self.conf.resizes {
            let values: Vec<&str> = resize_string.split(&self.conf.delimiter).collect();
            let _dir = values[0];
            let amp = values[1];
            let pane = values[2];
            resize_up(pane, amp);
        }
    }

    fn ssh_panes(&self) {
        if let Some(address) = self.conf.get_addr() {
            let ssh_cmd = format!("ssh {}", address);
            for i in 1..(self.nb_panes + 1) {
                tmux_cmd!(i.to_string(), &ssh_cmd);
            }
            thread::sleep(WAIT_MILIS);
            tmux_rename!(address);
            self.clear_panes();
        }
    }

    fn run_commands(&self) {
        for cmd_string in &self.conf.commands {
            let values: Vec<&str> = cmd_string.split(&self.conf.delimiter).collect();
            let command = values[0];
            let pane_num = values[1];
            tmux_cmd!(pane_num, command);
        }
        thread::sleep(WAIT_MILIS);
    }

    fn clear_panes(&self) {
        for pane in 1..(self.nb_panes + 1) {
            tmux_cmd!(pane.to_string(), "clear");
        }
        thread::sleep(WAIT_MILIS);
    }
}
