use reachable::*;
use std::str::FromStr;

use crate::validation::validator_helper::is_valid_split;

use super::validator_helper::is_valid_command;

pub const LIST_SPLIT: &str = ",";

const DELIMITER_SIZE_MIN: usize = 1;
const DELIMITER_SIZE_MAX: usize = 5;

pub fn is_address_reachable(addr: &str) -> Result<(), String> {
    let target = IcmpTarget::from_str(addr).unwrap();
    if target.check_availability().is_err() {
        return Err(format!(
            "The address '{}' is not reachable",
            target.get_id()
        ));
    }
    Ok(())
}

pub fn is_valid_layout(layout: &str, delimiter: &str, nb_panes: usize) -> Result<(), Vec<String>> {
    let splits: Vec<String> = layout.split(LIST_SPLIT).map(String::from).collect();
    return validate_string_vector(splits, delimiter, nb_panes, is_valid_split);
}

pub fn is_valid_commands(
    cmds: &Vec<String>,
    delim: &str,
    nb_panes: usize,
) -> Result<(), Vec<String>> {
    return validate_string_vector(cmds.to_vec(), delim, nb_panes, is_valid_command);
}

fn validate_string_vector(
    to_validate: Vec<String>,
    delimiter: &str,
    nb_panes: usize,
    validator_fct: fn(&str, &str, usize) -> Result<(), String>,
) -> Result<(), Vec<String>> {
    let mut errors: Vec<String> = Vec::new();
    for obj in to_validate {
        if let Err(error) = validator_fct(&obj, delimiter, nb_panes) {
            errors.push(error);
        }
    }
    if !errors.is_empty() {
        return Err(errors);
    }
    return Ok(());
}

pub fn is_valid_delimiter(delimiter: &str) -> Result<(), String> {
    if delimiter.len() < DELIMITER_SIZE_MIN || delimiter.len() > DELIMITER_SIZE_MAX {
        return Err(format!(
            "The size of the delimiter {} should be between {} and {} included",
            delimiter, DELIMITER_SIZE_MIN, DELIMITER_SIZE_MAX,
        ));
    }
    return Ok(());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_valid_split() {
        assert!(is_valid_split("h~~1", "~~", 2).is_ok());
        assert!(is_valid_split("v~~9", "~~", 9).is_ok());
        assert!(is_valid_split("H~~1", "~~", 2).is_ok());
        assert!(is_valid_split("V~~9", "~~", 9).is_ok());
        assert!(is_valid_split("", "~~", 1).is_ok());
    }

    #[test]
    fn test_not_valid_split() {
        assert!(is_valid_split("a~~1", "~~", 2).is_err());
        assert!(is_valid_split("a~~1~~2", "~~", 9).is_err());
        assert!(is_valid_split("h~~10", "~~", 9).is_err());
        assert!(is_valid_split("~~1", "~~", 3).is_err());
        assert!(is_valid_split("v~~", "~~", 2).is_err());
        assert!(is_valid_split("1~~h", "~~", 2).is_err());
        assert!(is_valid_split("billy", "~~", 3).is_err());
        assert!(is_valid_split("h~~2", "##", 3).is_err());
    }
    #[test]
    fn test_valid_command() {
        assert!(is_valid_command("echo hello~~1", "~~", 2).is_ok());
        assert!(is_valid_command("echo hello##3", "##", 3).is_ok());
    }

    #[test]
    fn test_not_valid_command() {
        assert!(is_valid_command("echo hello~~1", "##", 2).is_err());
        assert!(is_valid_command("echo hello##3", "##", 2).is_err());
        assert!(is_valid_command("echo hello3", "~~", 2).is_err());
        assert!(is_valid_command("echo hello~~1~~2", "~~", 2).is_err());
    }
}
