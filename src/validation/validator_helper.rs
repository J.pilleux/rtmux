const PANE_NUM_MIN: usize = 1;

pub const HORIZONTAL: &str = "h";
pub const VERTICAL: &str = "v";

pub const RESIZE_UP: &str = "u";
pub const RESIZE_DOWN: &str = "d";
pub const RESIZE_LEFT: &str = "l";
pub const RESIZE_RIGHT: &str = "r";

const MAX_RESIZE_VALUE: i8 = 50;
const MIN_RESIZE_VALUE: i8 = 0;

pub const EXPECTED_LAYOUT_VALUE_COUNT: usize = 2;
pub const EXPECTED_RESIZE_VALUE_COUNT: usize = 3;

pub fn is_valid_pane_number(pane_number: &str, split: &str, nb_panes: usize) -> Result<(), String> {
    let pane_number = match pane_number.parse::<usize>() {
        Ok(value) => value,
        Err(..) => {
            return Err(format!(
                "The pane number {} in split {} is not an integer",
                pane_number, split
            ))
        }
    };

    if pane_number < PANE_NUM_MIN || pane_number > nb_panes {
        return Err(format!(
            "The pane number {} in split {} is not between {} and {}",
            pane_number, split, PANE_NUM_MIN, nb_panes
        ));
    }
    return Ok(());
}

pub fn is_valid_command(cmd: &str, delim: &str, nb_panes: usize) -> Result<(), String> {
    if let Err(error) = contains_delimiter(cmd, delim, "split") {
        return Err(error);
    }
    let values: Vec<&str> = cmd.split(&delim).collect();
    if values.len() != EXPECTED_LAYOUT_VALUE_COUNT {
        return Err(format!("Too many delimiter found in the command {}", cmd));
    }

    if let Err(error) = is_valid_pane_number(values[1], cmd, nb_panes) {
        return Err(error);
    }

    return Ok(());
}

pub fn is_valid_split(split: &str, delim: &str, nb_panes: usize) -> Result<(), String> {
    if let Err(error) = contains_delimiter(split, delim, "split") {
        return Err(error);
    }

    let values: Vec<&str> = split.split(&delim).collect();
    if values.len() != EXPECTED_LAYOUT_VALUE_COUNT {
        return Err(format!("Too many delimiter found in the split {}", split));
    }

    let valid_splits: Vec<&str> = vec![HORIZONTAL, VERTICAL];
    if !is_str_in_vector(values[0], valid_splits) {
        return Err(format!(
            "The direction {} in split {} is not valid",
            values[0], split
        ));
    }

    if let Err(error) = is_valid_pane_number(values[1], split, nb_panes) {
        return Err(error);
    }

    return Ok(());
}

pub fn is_valid_resize(resize: &str, delim: &str, nb_panes: usize) -> Result<(), String> {
    if let Err(error) = contains_delimiter(resize, delim, "resize") {
        return Err(error);
    }

    let values: Vec<&str> = resize.split(&delim).collect();
    if values.len() != EXPECTED_RESIZE_VALUE_COUNT {
        return Err(format!(
            "Too many delimiter found in the delimiter {}",
            resize
        ));
    }

    let valid_resizes: Vec<&str> = vec![RESIZE_UP, RESIZE_DOWN, RESIZE_LEFT, RESIZE_RIGHT];
    if !is_str_in_vector(values[0], valid_resizes) {
        return Err(format!(
            "The direction {} in resize {} is not valid",
            values[0], resize
        ));
    }

    let resize_value = match values[1].parse::<i8>() {
        Ok(value) => value,
        Err(..) => {
            return Err(format!(
                "The resize value {} in resize {} is not an integer",
                values[1], resize
            ))
        }
    };

    if resize_value < MIN_RESIZE_VALUE || resize_value > MAX_RESIZE_VALUE {
        return Err(format!(""))
    }

    if let Err(error) = is_valid_pane_number(values[2], resize, nb_panes) {
        return Err(error);
    }

    return Ok(());
}

fn contains_delimiter(str: &str, delim: &str, what: &str) -> Result<(), String> {
    if str.is_empty() {
        return Ok(());
    }

    if !str.contains(delim) {
        return Err(format!(
            "The {} {} does not contains the delimiter {}",
            what, str, delim
        ));
    }
    return Ok(());
}

fn is_str_in_vector(str: &str, values: Vec<&str>) -> bool {
    return values.iter().any(|&s| s == str.to_lowercase());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_valid_pane_number() {
        assert!(is_valid_pane_number("1", "", 2).is_ok());
        assert!(is_valid_pane_number("9", "", 10).is_ok());
        assert!(is_valid_pane_number("5", "", 6).is_ok());
    }

    #[test]
    fn test_not_valid_pane_number() {
        assert!(is_valid_pane_number("0", "", 1).is_err());
        assert!(is_valid_pane_number("10", "", 7).is_err());
        assert!(is_valid_pane_number("-1", "", 3).is_err());
        assert!(is_valid_pane_number("billy", "", 4).is_err());
    }

    #[test]
    fn test_valid_resize() {
        assert!(is_valid_resize("u~~10~~1", "~~", 3).is_ok());
        assert!(is_valid_resize("d~~10~~1", "~~", 3).is_ok());
        assert!(is_valid_resize("r~~10~~1", "~~", 3).is_ok());
        assert!(is_valid_resize("l~~10~~1", "~~", 3).is_ok());
        assert!(is_valid_resize("U~~10~~1", "~~", 3).is_ok());
        assert!(is_valid_resize("D~~10~~1", "~~", 3).is_ok());
        assert!(is_valid_resize("R~~10~~1", "~~", 3).is_ok());
        assert!(is_valid_resize("L~~10~~1", "~~", 3).is_ok());
        assert!(is_valid_resize("L~~15~~2", "~~", 3).is_ok());
    }

    #[test]
    fn test_not_valid_resize() {
        assert!(is_valid_resize("G~~10~~1", "~~", 3).is_err());
        assert!(is_valid_resize("G~~10~~1", "yy", 3).is_err());
        assert!(is_valid_resize("G~~10", "~~", 3).is_err());
        assert!(is_valid_resize("u~~55~~1", "~~", 3).is_err());
        assert!(is_valid_resize("u~~-1~~1", "~~", 3).is_err());
        assert!(is_valid_resize("u~~billy~~1", "~~", 3).is_err());
    }
}
