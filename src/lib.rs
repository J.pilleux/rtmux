pub mod arg_parsing;
pub mod tmux_wrapper;
pub mod tmux_commands;
pub mod conf;
pub mod validation;
