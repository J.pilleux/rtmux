extern crate rtmux;
extern crate tmux_interface;

use clap::ArgMatches;
use std::process;

use rtmux::{arg_parsing::parse_args, conf::TmuxConf, tmux_commands::TmuxCommand};

fn run_rtmux(args: ArgMatches) {
    TmuxConf::create_default_cfg();
    let conf = TmuxConf::new(args);
    let mut tmux = TmuxCommand::new(conf);

    tmux.run();
}

fn main() {
    let args = parse_args();

    if args.is_present("init") {
        TmuxConf::create_default_cfg();
        process::exit(0);
    }

    run_rtmux(args);
}
