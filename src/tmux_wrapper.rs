use tmux_interface::{SplitWindow, ResizePane};

const RESIZE_UP: &str = "u";
const RESIZE_DOWN: &str = "d";
const RESIZE_LEFT: &str = "l";
const RESIZE_RIGHT: &str = "r";

macro_rules! tmux_cmd {
    ($pane:expr,$cmd:expr) => {
        SendKeys::new()
            .target_pane($pane)
            .key($cmd)
            .key("C-m")
            .output()
            .unwrap();
    };
}

pub fn vsplit(pane_num: &str) {
    SplitWindow::new()
        .target_pane(pane_num)
        .vertical()
        .output()
        .unwrap();
}

pub fn hsplit(pane_num: &str) {
    SplitWindow::new()
        .target_pane(pane_num)
        .horizontal()
        .output()
        .unwrap();
}

pub fn resize_pane(pane_num: &str, adjustment: &str, direction: &str) {
    let dir = String::from(direction).to_lowercase();
    match dir.as_str() {
        RESIZE_UP => ResizePane::new().target_pane(pane_num).up().adjustment(adjustment),
        RESIZE_DOWN => ResizePane::new().target_pane(pane_num).up().adjustment(adjustment),
        RESIZE_LEFT => ResizePane::new().target_pane(pane_num).up().adjustment(adjustment),
        RESIZE_RIGHT => ResizePane::new().target_pane(pane_num).up().adjustment(adjustment),
        _ => eprintln!("The direction {} is not known", direction)
    };
}

pub fn resize_up(pane_num: &str, amplitude: &str) {
    ResizePane::new()
        .target_pane(pane_num)
        .up()
        .adjustment(amplitude)
        .output()
        .unwrap();
}

pub fn resize_down(pane_num: &str, amplitude: &str) {
    ResizePane::new()
        .target_pane(pane_num)
        .down()
        .adjustment(amplitude)
        .output()
        .unwrap();
}

pub fn resize_left(pane_num: &str, amplitude: &str) {
    ResizePane::new()
        .target_pane(pane_num)
        .left()
        .adjustment(amplitude)
        .output()
        .unwrap();
}

pub fn resize_right(pane_num: &str, amplitude: &str) {
    ResizePane::new()
        .target_pane(pane_num)
        .right()
        .adjustment(amplitude)
        .output()
        .unwrap();
}

macro_rules! tmux_rename {
    ($name:expr) => {
        RenameWindow::new().new_name($name).output().unwrap()
    };
}

pub(crate) use {tmux_cmd, tmux_rename};
