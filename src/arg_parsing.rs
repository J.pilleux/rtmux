use clap::{App, Arg, ArgMatches};

const INIT_HELP: &str = r#"Will initialize a default configuration at one of these destinations :
    - $XDG_CONFIG_HOME/rtmux/rtmux.json if XDG_CONFIG_HOME is set_term_width
    - $HOME/.rtmux.json otherwise"#;

const LAYOUT_HELP: &str = r#"Defines the way to split panes using a list of letters and numbers :
    - h means 'horizontal split'
    - v means 'vertical split'
    - The number following the letter indicates the pane number
    EXAMPLE: 'h~~1,v~~1' will split the first pane in horizontal and then in vertical
    NOTE: You can redefine what to une as delimiter (~~) with the -d option"#;

const DELIMITER_HELP: &str = r#"Changes the delimiter to use in different options :
    - command
    - layout"#;

const COMMAND_HELP: &str = r#"Defines a command to run in which pane with this syntax: CMD~~PANE_NUMBER
    NOTE: You can use multiple times this option to run multiple commands.
    NOTE: You can redefine what to une as delimiter (~~) with the -d option"#;

const RESIZE_HELP: &str = r#"Resize panes using this syntax : DIRECTION~~AMPLITUDE~~PANE
    - DIRECTION : Can be 'u', 'd', 'l' or 'r' to choose a direction
    - AMPLITUDE : An integer (between 1 and 30 is recommended). Please refer to tmux manual for more precisions
    - PANE      : An integer, pane number to resize
    EXAMPLE: u~~10~~2
    NOTE: You can use multiple times this option to run multiple commands.
    NOTE: You can redefine what to une as delimiter (~~) with the -d option"#;

pub fn parse_args() -> ArgMatches<'static> {
    App::new("rtmux")
        .version("0.1.0")
        .author("Julien PILLEUX")
        .about("A Tmux command line utility")
        .set_term_width(120)
        .arg(
            Arg::with_name("addr")
                .value_name("addr")
                .help("The address (IP or hostname) to ssh to in all spawned panes")
                .multiple(false),
        )
        .arg(
            Arg::with_name("init")
                .short("i")
                .long("init")
                .takes_value(false)
                .help(INIT_HELP),
        )
        .arg(
            Arg::with_name("editor")
                .short("e")
                .long("editor")
                .takes_value(true)
                .value_name("EDITOR_CMD")
                .help("Specify the command to run an editor"),
        )
        .arg(
            Arg::with_name("layout")
                .short("l")
                .long("layout")
                .takes_value(true)
                .value_name("LAYOUT")
                .help(LAYOUT_HELP),
        )
        .arg(
            Arg::with_name("delimiter")
                .short("d")
                .long("delimiter")
                .takes_value(true)
                .value_name("DELIM")
                .help(DELIMITER_HELP)
        )
        .arg(
            Arg::with_name("command")
                .short("c")
                .long("command")
                .takes_value(true)
                .value_name("COMMAND")
                .multiple(true)
                .help(COMMAND_HELP)
            )
        .arg(
            Arg::with_name("resize")
                .short("r")
                .long("resize")
                .takes_value(true)
                .value_name("RESIZE")
                .multiple(true)
                .help(RESIZE_HELP)
            )
        .get_matches()
}
