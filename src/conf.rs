use clap::ArgMatches;
use serde::{Deserialize, Serialize};
use std::{
    env,
    fs::{self, create_dir, File},
    io::Write,
    path::Path,
    process,
};

use crate::validation::validator::{is_address_reachable, is_valid_commands, is_valid_delimiter, is_valid_layout};

const CFG_FILE_NAME: &str = "rtmux.json";

fn get_default_folder() -> String {
    let cfg_home = match env::var("XDG_CONFIG_HOME") {
        Ok(path) => {
            let config_dir = format!("{}/rtmux/", &path);
            if !Path::new(&config_dir).exists() {
                create_dir(&config_dir).expect("Cannot create directories");
            }
            config_dir + CFG_FILE_NAME
        }
        Err(..) => format!("{}/.{}", env::var("HOME").unwrap(), CFG_FILE_NAME),
    };
    return cfg_home;
}

fn dft_nb_splits() -> usize {
    4
}

fn dft_addr() -> Option<String> {
    None
}

fn dft_layout() -> String {
    String::from("h~~1,v~~1,v~~3")
}

fn dft_delimiter() -> String {
    String::from("~~")
}

fn dft_commands() -> Vec<String> {
    Vec::new()
}

fn dft_resizes() -> Vec<String> {
    Vec::new()
}

#[derive(Deserialize, Serialize, Debug)]
pub struct TmuxConf {
    #[serde(skip_serializing)]
    pub nb_splits: usize,

    #[serde(default = "dft_addr")]
    pub address: Option<String>,

    #[serde(default = "dft_layout")]
    pub layout: String,

    #[serde(default = "dft_delimiter")]
    pub delimiter: String,

    #[serde(default = "dft_commands")]
    pub commands: Vec<String>,

    #[serde(default = "dft_resizes")]
    pub resizes: Vec<String>,
}

impl TmuxConf {
    pub fn new(args: ArgMatches) -> TmuxConf {
        TmuxConf::create_default_cfg();
        let mut conf = TmuxConf::load_from_file();


        if let Some(addr) = args.value_of("addr") {
            conf.address = Some(String::from(addr));
        }

        if let Some(layout) = args.value_of("layout") {
            conf.layout = String::from(layout);
        }

        conf.nb_splits = conf.layout.matches(",").count() + 1;

        if let Some(delimiter) = args.value_of("delimiter") {
            conf.delimiter = String::from(delimiter);
        }

        if let Some(commands) = args.values_of("command") {
            conf.commands = commands.map(String::from).collect();
        }

        if let Some(resizes) = args.values_of("resizes") {
            conf.resizes = resizes.map(String::from).collect();
        }

        conf.validate();
        return conf;
    }

    fn new_default() -> TmuxConf {
        return TmuxConf {
            nb_splits: dft_nb_splits(),
            address: dft_addr(),
            layout: dft_layout(),
            delimiter: dft_delimiter(),
            commands: dft_commands(),
            resizes: dft_resizes(),
        };
    }

    fn load_from_file() -> TmuxConf {
        let path = get_default_folder();
        let content = fs::read_to_string(path).expect("Cannot read configuration from file");
        return serde_json::from_str(&content).expect("Cannot deserialize configuration file");
    }

    pub fn create_default_cfg() {
        let path = get_default_folder();
        if !Path::new(&path).exists() {
            let conf = TmuxConf::new_default();
            let content =
                serde_json::to_string(&conf).expect("Cannot deserialize default configuration");
            let mut file = File::create(&path).expect("Cannot create file");
            file.write_all(content.as_bytes())
                .expect("Cannot write default file");
            println!("File creted at : {}", &path);
        }
    }

    pub fn get_addr(&self) -> Option<&String> {
        return self.address.as_ref();
    }

    pub fn validate(&self) {
        let mut errors: Vec<String> = Vec::new();
        let addr = match &self.address {
            Some(addr) => addr,
            None => "UNKNOWN",
        };

        if let Err(error) = is_address_reachable(&addr) {
            errors.push(error)
        };

        if let Err(errs) = is_valid_layout(&self.layout, &self.delimiter, self.nb_splits) {
            errors.extend(errs);
        }

        if let Err(error) = is_valid_delimiter(&self.delimiter) {
            errors.push(error);
        }

        if let Err(errs) = is_valid_commands(&self.commands, &self.delimiter, self.nb_splits) {
            errors.extend(errs);
        }

        if ! errors.is_empty() {
            TmuxConf::print_errors(&errors);
            process::exit(1);
        }
    }

    fn print_errors(errors: &Vec<String>) {
        eprintln!("ERROR : Configuration errors detected :");
        for err in errors {
            eprintln!("  - {}", err)
        }
    }
}
