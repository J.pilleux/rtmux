use rtmux::validation::validator::{is_valid_commands, is_valid_delimiter, is_valid_layout};

fn to_str_vec(str_vec: Vec<&str>) -> Vec<String> {
    let mut result: Vec<String> = Vec::new();
    for str in str_vec {
        result.push(String::from(str));
    }
    return result;
}

#[test]
fn test_is_valid_layout_valid() {
    assert!(is_valid_layout("h~~1,v~~2", "~~", 2).is_ok());
    assert!(is_valid_layout("h~~1", "~~", 1).is_ok());
    assert!(is_valid_layout("", "~~", 1).is_ok());
    assert!(is_valid_layout("h~~2,", "~~", 2).is_ok());
}

#[test]
fn test_is_valid_layout_invalid() {
    assert!(is_valid_layout("h~~2,v~~7", "@@", 8).is_err());
    assert!(is_valid_layout("h~~2,v~~7", "", 8).is_err());
    assert!(is_valid_layout("h~~10,v~~7", "~~", 9).is_err());
    assert!(is_valid_layout("h~~3,k~~7", "~~", 8).is_err());
    assert!(is_valid_layout("h~~1,v##7", "~~", 8).is_err());
    assert!(is_valid_layout("billy", "[", 2).is_err());
    assert!(is_valid_layout("h~~1,v~~5", "~~", 2).is_err());
}

#[test]
fn test_is_valid_delimiter_valid() {
    assert!(is_valid_delimiter("~~").is_ok());
    assert!(is_valid_delimiter("@").is_ok());
    assert!(is_valid_delimiter("-|-").is_ok());
}

#[test]
fn test_is_valid_delimiter_invalid() {
    assert!(is_valid_delimiter("").is_err());
    assert!(is_valid_delimiter("~~~~~~").is_err());
}

#[test]
fn test_is_valid_commands_valid() {
    assert!(is_valid_commands(&to_str_vec(vec!["abc~~1"]), "~~", 1).is_ok());
    assert!(is_valid_commands(&to_str_vec(vec!["abc~~1", "def~~2"]), "~~", 3).is_ok());
}

#[test]
fn test_is_valid_commands_invalid() {
    assert!(is_valid_commands(&to_str_vec(vec!["abc~~2"]), "~~", 1).is_err());
    assert!(is_valid_commands(&to_str_vec(vec!["abc~~1", "def##2"]), "~~", 3).is_err());
}
