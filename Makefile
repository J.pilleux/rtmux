
VERSION := $(shell rgrep "^version" Cargo.toml | tr -s ' ' | cut -d ' ' -f 3 | sed 's/"//g')
TARGET := $(shell rgrep "^name" Cargo.toml | tr -s ' ' | cut -d ' ' -f 3 | sed 's/"//g')

ARCHIVE_NAME := "linux_x86_$(TARGET)_$(VERSION).tar.gz"
TARGET_PATH := "target/release/$(TARGET)"

all: archive

archive:
	@cargo build --release
	@cp $(TARGET_PATH) .
	@tar cf $(ARCHIVE_NAME) $(TARGET)
	@echo "Release available: $(ARCHIVE_NAME)"

clean:
	rm $(ARCHIVE_NAME) $(TARGET)
